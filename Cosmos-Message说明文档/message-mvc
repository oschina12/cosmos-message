message-mvc
===

> 对spring mvc的一个扩展

1. 配置
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:message-mvc="http://www.message.com/schema/message-mvc"
       xsi:schemaLocation="
  http://www.springframework.org/schema/beans   
  http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
  http://www.springframework.org/schema/context
    http://www.springframework.org/schema/context/spring-context-2.5.xsd
    http://www.message.com/schema/message-mvc
  http://www.message.com/schema/message-mvc/message-mvc-1.0.xsd">

    <context:component-scan base-package="solar.web,solar.*.web"/>

    <!-- tiles 配置 start -->
    <!-- 与传统的springMVC和tiles配置一致 -->
    <!-- 配置tiles页面解析器-->
    <bean id="viewResolver" class="org.springframework.web.servlet.view.UrlBasedViewResolver">
        <property name="viewClass" value="org.springframework.web.servlet.view.tiles3.TilesView"/>
    </bean>

    <bean id="localeResolver" class="org.springframework.web.servlet.i18n.CookieLocaleResolver"/>

    <!-- tiles配置器-->
    <bean id="tilesConfigurer" class="org.springframework.web.servlet.view.tiles3.TilesConfigurer">
        <property name="definitions">
            <list>
                <value>/WEB-INF/tiles-defs/main.xml</value>
            </list>
        </property>
        <property name="preparerFactoryClass" value="org.springframework.web.servlet.view.tiles3.SpringBeanPreparerFactory"/>
    </bean>
    <!-- tiles 配置 end -->

    <message-mvc:mvc>
        <!-- 配置web请求参数的封装,详见**备注1** -->
        <message-mvc:argument-resolvers>
            <bean class="message.security.annotation.resolver.CurrentAccountResolver" />
        </message-mvc:argument-resolvers>
        <!-- 配置web请求的参数转换成对象的转换器,详见**备注2** -->
        <message-mvc:converters>
            <bean class="message.mvc.convert.EnumConverterFactory"/>
        </message-mvc:converters>
        <!-- 配置拦截器 -->
        <message-mvc:interceptors>
            <bean class="message.security.core.LoginInterceptor"/>
        </message-mvc:interceptors>
        <!-- 配置spring restful的swagger服务 -->
        <message-mvc:swagger swaggerEnable="true"/>
    </message-mvc:mvc>
</beans>
```

备注1
---

> 1. 这里是将web请求中得参数封装变成controller中方法的参数
> 2. `message-mvc`共提供了4个解析器
> 3. 都在包`message.mvc.resolver`中

- `BindDataHandlerMethodArgumentResolver`将web请求中的参数绑定到一个对象中
    
    1. 平时我们在保存的时候,可能会同时同前台传递来两个对象的值,而且这两个对象有可能存在相同的字段,比如pkId,这时候,springMVC自带的`bind()`方法就不能够满足这个需求了.这样就需要`BindDataHandlerMethodArgumentResolver`的出现了.
    2. 在controller的方法中,我们这样写`test(@Bind("p1") Pojo1 pojo1, @Bind("p2") Pojo2 pojo2)`
    3. 前台请求中的参数应该是这样的`p1.pkId=1&p2.pkId=2`
    4. 这样两个pkId都能正确的bind到各个pojo对象中
    
- `ParseDataHandlerMethodArgumentResolver` 当前台传入类似于1,2,3,4,5之类的字符串，将会被解析成一个List
    
    1. 前台传入如下的参数`ids=1,2,3,4`,或者`ids=1|2|3|4`
    2. 后台controller的方法中`test(@Parse(dest = Long.class, required = true, delimiters = "|") List<Long> tagIds)`
    3. 指定目标类型为`Long`,并且是必填的,且分隔符为`|`,默认分隔符为`,`
    
- `WebInputHandlerMethodArgumentResolver`和`WebOutputHandlerMethodArgumentResolver`
    
    1. 将web请求的HttpServletRequest和HttpServletResponse封装成WebInput和WebOutput
    2. 文件上传是封装成FileWebInput
    

备注2
---

> 1. 类型转换器,默认提供两个
> 2. 都在包`message.mvc.convert`中
> 3. `EnumConverterFactory`  、`StringDateConvertFactory`

1. `EnumConverterFactory`,如前台传入枚举,后台将会把传入的字符串类型变换成枚举类型
    
    如果请求参数为`deleteFlag=1`,通过这个转换器,将会变成`DeleteFlag.DELETE_YES`

2. `StringDateConvertFactory`,将前台传来的`String`类型的参数转变成`Date`类型的参数

    如请求参数为`date=2015-05-13`,将会变成此值的`java.util.Date`类型

## 其他一些工具类的介绍
1. `message.mvc.debug.DebugRequestListener`
    - 系统是否开启debug功能,可以查看请求的详情
    - 只需在xml中配置
    ```xml
    <!-- 配置开启debug -->
    <bean class="message.mvc.debug.DebugRequestListener"/>
    ```
    - 效果
    ```
    request process info:
    begin-----------------
    time=[1234]
    url=[/home/index]
    client=[127.0.0.1]
    method=[POST]
    end-------------------
    ```

##依赖的cosmos项目
- message-logger
- message-json
- message-template

